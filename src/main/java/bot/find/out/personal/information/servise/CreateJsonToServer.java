package bot.find.out.personal.information.servise;

import bot.find.out.personal.information.entity.ReportJson;
import com.google.gson.Gson;

public class CreateJsonToServer {
    private Gson gson = new Gson();
    private String json;

    public CreateJsonToServer(ReportJson reportJson) {
        json = gson.toJson(reportJson);
    }

    public String getJson(){
        return json;
    }
}
