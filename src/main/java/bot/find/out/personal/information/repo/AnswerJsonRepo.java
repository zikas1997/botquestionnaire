package bot.find.out.personal.information.repo;

import bot.find.out.personal.information.entity.AnswerToQuestion;
import bot.find.out.personal.information.entity.ReportJson;

public interface AnswerJsonRepo {
    void addAnswerToQuestion(AnswerToQuestion answerToQuestion);
    boolean deleteAnswerToQuestion(String token);
    ReportJson selectReportJson(String token);
    void deleteUserAnswer(String token);
}
