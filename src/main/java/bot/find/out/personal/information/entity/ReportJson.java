package bot.find.out.personal.information.entity;

import java.util.ArrayList;
import java.util.List;

public class ReportJson {

    private String token;
    private List<String[]> answer = new ArrayList<String[]>();


    public ReportJson() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<String[]> getAnswer() {
        return answer;
    }

    public void setAnswer(List<String[]> answer) {
        this.answer = answer;
    }


    public String toString(String tokenId) {
        String question = "";
        String question1 = "";
        for(String[] list : answer){
            question = question+ "Вопрос №";
            for (int i=0;i<list.length;i++){
                if (i==0)
                    question1 = question1+ list[i] + " ";
                if(i==2)
                    question1 = question1+ "Вопрос: "+list[i] + "\n ";
                if(i==3){
                    question1 = question1+"Ответ: "+ list[i] + ".";
                }
            }
            question = question+ question1 +"\n";
            question1="";
        }
        return question;
    }
}
