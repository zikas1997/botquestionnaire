package bot.find.out.personal.information.repo;

import bot.find.out.personal.information.entity.User;

import java.util.ArrayList;
import java.util.List;


public class UserPersonalInformationRepository implements PersonalInformationRepo {

    List<User> users;

    public UserPersonalInformationRepository() {
        users = new ArrayList<User>();
    }

    public void addUser(User user) {
        users.add(user);
    }

    public void updateUser(User user) {
        for(User tempUser : users){
            if(tempUser.getChatId()== user.getChatId()){
                tempUser.clone(user);
            }
        }
    }

    public User selectUser(long chatId) {
        for(User user : users){
            if(user.getChatId()==chatId){
                return user;
            }
        }
        return null;
    }

    public void deleteUser(long chatId) {
        long id = selectUser(chatId).getChatId();
        String token= selectUser(chatId).getUserToken();
        users.remove(selectUser(chatId));
        User user = new User();
        user.setUserToken(token);
        user.setChatId(id);
        users.add(user);
    }

}
