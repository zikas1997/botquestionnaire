package bot.find.out.personal.information.entity;

import java.util.ArrayList;
import java.util.List;

public class Questionnaire {

    private String nameQuestionnaire;
    private List<String[]> question = new ArrayList<String[]>();

    public Questionnaire(String nameQuestionnaire, List<String[]> question) {
        this.nameQuestionnaire = nameQuestionnaire;
        this.question = question;
    }

    public String getNameQuestionnaire() {
        return nameQuestionnaire;
    }

    public List<String[]> getQuestion() {
        return question;
    }
}
