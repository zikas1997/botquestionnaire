package bot.find.out.personal.information.repo;

import bot.find.out.personal.information.entity.AnswerToQuestion;
import bot.find.out.personal.information.entity.ReportJson;

import java.util.ArrayList;
import java.util.List;

public class AnswerJsonRepository implements AnswerJsonRepo {

    private List<ReportJson> reportJsons;
    ReportJson reportJson;
    public AnswerJsonRepository() {
        reportJsons = new ArrayList<ReportJson>();
    }


    public void addAnswerToQuestion(AnswerToQuestion answerToQuestion) {
        reportJson = new ReportJson();
        if((reportJsons.size()==0)||(selectReportJson(answerToQuestion.getToken())==null)){
            reportJson.setToken(answerToQuestion.getToken());
            reportJsons.add(reportJson);
        }
        for(ReportJson reportJson : reportJsons){
            if(reportJson.getToken().equals(answerToQuestion.getToken())){
                reportJson.getAnswer().add(new String[]{answerToQuestion.getNumberQuestion(),answerToQuestion.getTypeQuestion(),answerToQuestion.getTextQuestion(),answerToQuestion.getAnswerQuestion()});
            }
        }
    }

    public boolean deleteAnswerToQuestion(String token) {
        for(ReportJson reportJson : reportJsons){
            boolean temp = reportJson.getToken().equals(token);
            int temp2 = reportJson.getAnswer().size() -1;
            if(temp){
                reportJson.getAnswer().remove(temp2);
                return true;
            }
        }
        return false;
    }

    public ReportJson selectReportJson(String token) {
        for(ReportJson reportJson : reportJsons){
            if(reportJson.getToken().equals(token)){
                return reportJson;
            }
        }
        return null;
    }

    public void deleteUserAnswer(String token) {
        reportJsons.remove(selectReportJson(token));
    }

    public List<ReportJson> getReportJsons() {
        return reportJsons;
    }
}
