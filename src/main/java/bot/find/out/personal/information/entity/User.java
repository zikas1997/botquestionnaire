package bot.find.out.personal.information.entity;

public class User {

    private long chatId;
    private String userToken;
    private String questionnaireName = "";
    private String lastMessageBot;
    private String lastMessageUser;
    private String questionNumber;
    private boolean checkConsent; // проверка согласия на прохождение теста/ответа на текстовые сообщения
    private boolean checkStartTest;
    private boolean checkEndTest;

    public User() {}

    public long getChatId() {
        return chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getQuestionnaireName() {
        return questionnaireName;
    }

    public void setQuestionnaireName(String questionnaireName) {
        this.questionnaireName = questionnaireName;
    }

    public String getLastMessageBot() {
        return lastMessageBot;
    }

    public void setLastMessageBot(String lastMessageBot) {
        this.lastMessageBot = lastMessageBot;
    }

    public String getLastMessageUser() {
        return lastMessageUser;
    }

    public void setLastMessageUser(String lastMessageUser) {
        this.lastMessageUser = lastMessageUser;
    }

    public boolean isCheckConsent() {
        return checkConsent;
    }

    public void setCheckConsent(boolean checkConsent) {
        this.checkConsent = checkConsent;
    }

    public boolean isCheckEndTest() {
        return checkEndTest;
    }

    public void setCheckEndTest(boolean checkEndTest) {
        this.checkEndTest = checkEndTest;
    }

    public boolean isCheckStartTest() {
        return checkStartTest;
    }

    public void setCheckStartTest(boolean checkStartTest) {
        this.checkStartTest = checkStartTest;
    }

    public String getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(String questionNumber) {
        this.questionNumber = questionNumber;
    }

    public void clone(User user){
        chatId = user.getChatId();
        userToken = user.getUserToken();
        questionnaireName = user.getQuestionnaireName();
        lastMessageBot = user.getLastMessageBot();
        lastMessageUser = user.getLastMessageUser();
        questionNumber = user.getQuestionNumber();
        checkConsent = user.isCheckConsent();
        checkStartTest = user.isCheckStartTest();
        checkEndTest = user.isCheckEndTest();
    }
    public void updaterUser(User user){
        userToken = user.getUserToken();
        questionnaireName = user.getQuestionnaireName();
        questionNumber = user.getQuestionNumber();
        checkConsent = user.isCheckConsent();
        checkStartTest = user.isCheckStartTest();
        checkEndTest = user.isCheckEndTest();
    }

    @Override
    public String toString() {
        return "User{" +
                "chatId=" + chatId +
                ", userToken='" + userToken + '\'' +
                ", questionnaireName='" + questionnaireName + '\'' +
                ", lastMessageBot='" + lastMessageBot + '\'' +
                ", lastMessageUser='" + lastMessageUser + '\'' +
                ", questionNumber=" + questionNumber +
                ", checkConsent=" + checkConsent +
                ", checkStartTest=" + checkStartTest +
                ", checkEndTest=" + checkEndTest +
                '}';
    }
}
