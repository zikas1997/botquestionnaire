package bot.find.out.personal.information.servise;

import bot.find.out.personal.information.entity.AnswerToQuestion;
import bot.find.out.personal.information.entity.Questionnaire;
import bot.find.out.personal.information.entity.ReportJson;
import bot.find.out.personal.information.entity.User;
import bot.find.out.personal.information.repo.AnswerJsonRepository;
import com.google.gson.Gson;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PassingQuestionnaires {

    //----------------------------------------------------------------------------------------------
    private SendMessage sendMessage;
    private InlineKeyboardMarkup markupInline;
    private List<List<InlineKeyboardButton>> rowsInline;
    private List<InlineKeyboardButton> rowInline;

    //----------------------------------------------------------------------------------------------

    private User updateUser;
    private String typeQuestion;
    private String checkAnswer = "Вы уверены в вашем ответе?";

    private List<Questionnaire> questionnaires = new ArrayList<Questionnaire>();
    private Questionnaire questionnaire;
    private boolean check = false;

    private AnswerJsonRepository answerJsonRepository = new AnswerJsonRepository();
    private ReportJson tempReportJson;
    private AnswerToQuestion answerToQuestion;

    private Gson gson = new Gson();

    //----------------------------------------------------------------------------------------------

    public PassingQuestionnaires() {
    }

    //--------Метод создания базы анкет---------------------------------------------------------------------------
    public void addQuestionnaires(String json) {
        questionnaire = gson.fromJson(json, Questionnaire.class);
        questionnaires.add(questionnaire);
    }
    //--------Метод подготавливает сообзение пользователю и сохраняет в временное хранилище ответы-----------------------------------------------------------------------

    public void checkWhyQuestion(User user) {
        check = true;
        fieldInitializer(user);


        if (isQuestionFound(user)) {//Вопрос найден


            if (check)
                if (isText()) {
                    typeQuestion = "text";
                    if ((checkQuestion(updateUser.getLastMessageBot()) ||
                            (updateUser.getLastMessageBot().equals("Вы прошли анкетирование. Мы готовы отправить тест. Вы уверены, что хотите отправить анкету?")))) {
                        if (updateUser.getLastMessageUser().equals("trueIwant")
                                || updateUser.getLastMessageUser().equals("ОтправитьQuestionnaires")
                                || updateUser.getLastMessageUser().equals("ПройтизановоQuestionnaires")) {
                            if (!updateUser.isCheckEndTest()) {
                                updateUser.setQuestionNumber(String.valueOf(Integer.parseInt(updateUser.getQuestionNumber()) + 1));
                                updateUser.setLastMessageBot(getQuestion()[2]);

                                getQuestionAndPreparationSendMessage();
                                check = false;
                            } else {
                                if (updateUser.isCheckConsent() == false) {
                                    //Кнопки выбора
                                    rowInline.add(new InlineKeyboardButton().setText("Отправить анкету").setCallbackData("ОтправитьQuestionnaires"));
                                    rowInline.add(new InlineKeyboardButton().setText("Пройти анкету заного").setCallbackData("ПройтизановоQuestionnaires"));
                                    rowsInline.add(rowInline);
                                    markupInline.setKeyboard(rowsInline);
                                    sendMessage.setReplyMarkup(markupInline);
                                    //Задаем вопрос checkAnswer
                                    String endQuestionnaires = "Вы прошли анкетирование. Мы готовы отправить тест. Вы уверены, что хотите отправить анкету?";
                                    sendMessage.setText(endQuestionnaires);
                                    updateUser.setLastMessageBot(endQuestionnaires);
                                }
                                if (updateUser.getLastMessageUser().equals("ОтправитьQuestionnaires")) {
                                    CreateJsonToServer createJsonToServer = new CreateJsonToServer(answerJsonRepository.selectReportJson(user.getUserToken()));
                                    sendMessage.setText(answerJsonRepository.selectReportJson(user.getUserToken()).toString(user.getUserToken()));//Все!
                                    answerJsonRepository.deleteUserAnswer(updateUser.getUserToken());
                                }
                                if (updateUser.getLastMessageUser().equals("ПройтизановоQuestionnaires")) {
                                    sendMessage.setText("Есди хотите пройти тест заново, введите любое сообщение.");
                                    answerJsonRepository.deleteUserAnswer(updateUser.getUserToken());
                                }
                                updateUser.setCheckConsent(true);
                            }
                        } else {
                            //Удаляем последний Json
                            answerJsonRepository.deleteAnswerToQuestion(updateUser.getUserToken());
                            updateUser.setLastMessageBot(getQuestion()[2]);
                            sendMessage.setText(getQuestionAndPreparationSendMessage());
                        }
                    } else {
                        //Записываем в Json ответ(хранитья временно, если пользователь не подвердит мы удаляем)
                        setAnswerToQuestion(user.getQuestionNumber(), typeQuestion, user.getLastMessageBot(), user.getLastMessageUser());
                        answerJsonRepository.addAnswerToQuestion(answerToQuestion);
                        //Кнопки выбора
                        rowInline.add(new InlineKeyboardButton().setText("Да").setCallbackData("trueIwant"));
                        rowInline.add(new InlineKeyboardButton().setText("Нет, я ошибся").setCallbackData("falseIwant"));
                        rowsInline.add(rowInline);
                        markupInline.setKeyboard(rowsInline);
                        sendMessage.setReplyMarkup(markupInline);
                        //Задаем вопрос checkAnswer
                        sendMessage.setText(checkAnswer);
                        updateUser.setLastMessageBot(checkAnswer);
                        check = false;

                    }
                }
            if (check)
                if (isNumber()) {
                    typeQuestion = "number";

                    if ((checkQuestion(updateUser.getLastMessageBot()) ||
                            (updateUser.getLastMessageBot().equals("Вы прошли анкетирование. Мы готовы отправить тест. Вы уверены, что хотите отправить анкету?")))) {
                        if (updateUser.getLastMessageUser().equals("trueIwant")
                                || updateUser.getLastMessageUser().equals("ОтправитьQuestionnaires")
                                || updateUser.getLastMessageUser().equals("ПройтизановоQuestionnaires")) {
                            if (!updateUser.isCheckEndTest()) {
                                updateUser.setQuestionNumber(String.valueOf(Integer.parseInt(updateUser.getQuestionNumber()) + 1));
                                updateUser.setLastMessageBot(getQuestion()[2]);

                                getQuestionAndPreparationSendMessage();
                                check = false;
                            } else {
                                if (updateUser.isCheckConsent() == false) {
                                    //Кнопки выбора
                                    rowInline.add(new InlineKeyboardButton().setText("Отправить анкету").setCallbackData("ОтправитьQuestionnaires"));
                                    rowInline.add(new InlineKeyboardButton().setText("Пройти анкету заного").setCallbackData("ПройтизановоQuestionnaires"));
                                    rowsInline.add(rowInline);
                                    markupInline.setKeyboard(rowsInline);
                                    sendMessage.setReplyMarkup(markupInline);
                                    //Задаем вопрос checkAnswer
                                    String endQuestionnaires = "Вы прошли анкетирование. Мы готовы отправить тест. Вы уверены, что хотите отправить анкету?";
                                    sendMessage.setText(endQuestionnaires);
                                    updateUser.setLastMessageBot(endQuestionnaires);
                                }
                                if (updateUser.getLastMessageUser().equals("ОтправитьQuestionnaires")) {
                                    CreateJsonToServer createJsonToServer = new CreateJsonToServer(answerJsonRepository.selectReportJson(user.getUserToken()));
                                    sendMessage.setText(answerJsonRepository.selectReportJson(user.getUserToken()).toString(user.getUserToken()));//Все!
                                    answerJsonRepository.deleteUserAnswer(updateUser.getUserToken());
                                }
                                if (updateUser.getLastMessageUser().equals("ПройтизановоQuestionnaires")) {
                                    sendMessage.setText("Есди хотите пройти тест заново, введите любое сообщение.");
                                    answerJsonRepository.deleteUserAnswer(updateUser.getUserToken());
                                }
                                updateUser.setCheckConsent(true);
                            }
                        } else {
                            //Удаляем последний Json
                            answerJsonRepository.deleteAnswerToQuestion(updateUser.getUserToken());
                            updateUser.setLastMessageBot(getQuestion()[2]);
                            sendMessage.setText(getQuestionAndPreparationSendMessage());
                        }
                    } else {
                        try {
                            float isFloat = Float.parseFloat(updateUser.getLastMessageUser());
                            //Записываем в Json ответ(хранитья временно, если пользователь не подвердит мы удаляем)
                            setAnswerToQuestion(user.getQuestionNumber(), typeQuestion, user.getLastMessageBot(), user.getLastMessageUser());
                            answerJsonRepository.addAnswerToQuestion(answerToQuestion);
                            //Кнопки выбора
                            rowInline.add(new InlineKeyboardButton().setText("Да").setCallbackData("trueIwant"));
                            rowInline.add(new InlineKeyboardButton().setText("Нет, я ошибся").setCallbackData("falseIwant"));
                            rowsInline.add(rowInline);
                            markupInline.setKeyboard(rowsInline);
                            sendMessage.setReplyMarkup(markupInline);
                            //Задаем вопрос checkAnswer
                            sendMessage.setText(checkAnswer);
                            updateUser.setLastMessageBot(checkAnswer);
                            check = false;
                        } catch (NumberFormatException e) {
                            sendMessage.setText("Извините вы ввели не коректные данныые н вопрос \"" + getQuestion()[2] + "\"" + " Введите ответ заново");
                        }


                    }


                }

            if (check)
                if (isButton()) {
                    typeQuestion = "button";
                    if (updateUser.isCheckEndTest()) {
                        if (updateUser.isCheckConsent() == false) {
                            setAnswerToQuestion(user.getQuestionNumber(), typeQuestion, user.getLastMessageBot(),
                                    getQuestionsList().get(Integer.parseInt(user.getQuestionNumber()) - 1)[Integer.parseInt(user.getLastMessageUser())]);
                            answerJsonRepository.addAnswerToQuestion(answerToQuestion);
                            //Кнопки выбора
                            rowInline.add(new InlineKeyboardButton().setText("Отправить анкету").setCallbackData("ОтправитьQuestionnaires"));
                            rowInline.add(new InlineKeyboardButton().setText("Пройти анкету заного").setCallbackData("ПройтизановоQuestionnaires"));
                            rowsInline.add(rowInline);
                            markupInline.setKeyboard(rowsInline);
                            sendMessage.setReplyMarkup(markupInline);
                            //Задаем вопрос checkAnswer
                            String endQuestionnaires = "Вы прошли анкетирование. Мы готовы отправить тест. Вы уверены, что хотите отправить анкету?";
                            sendMessage.setText(endQuestionnaires);
                            updateUser.setLastMessageBot(endQuestionnaires);
                        }
                        if (updateUser.getLastMessageUser().equals("ОтправитьQuestionnaires")) {
                            CreateJsonToServer createJsonToServer = new CreateJsonToServer(answerJsonRepository.selectReportJson(user.getUserToken()));
                            sendMessage.setText(answerJsonRepository.selectReportJson(user.getUserToken()).toString(user.getUserToken()));//Все!
                            answerJsonRepository.deleteUserAnswer(updateUser.getUserToken());
                        }
                        if (updateUser.getLastMessageUser().equals("ПройтизановоQuestionnaires")) {
                            sendMessage.setText("Есди хотите пройти тест заново, введите любое сообщение.");
                            answerJsonRepository.deleteUserAnswer(updateUser.getUserToken());
                        }
                        updateUser.setCheckConsent(true);
                    } else {
                        setSendMessage(getQuestionsList().get(Integer.parseInt(user.getQuestionNumber()))[2]);//Задаем следующий вопрос
                        setAnswerToQuestion(user.getQuestionNumber(), typeQuestion, user.getLastMessageBot(),
                                getQuestionsList().get(Integer.parseInt(user.getQuestionNumber()) - 1)[Integer.parseInt(user.getLastMessageUser())]);
                        answerJsonRepository.addAnswerToQuestion(answerToQuestion);
                        updateUser.setLastMessageBot(sendMessage.getText());
                        updateUser.setQuestionNumber(getQuestionsList().get(Integer.parseInt(user.getQuestionNumber()))[0]);
                        check = false;

                    }

                }
            if (getQuestionsList().size() == (Integer.parseInt(updateUser.getQuestionNumber()))) {
                updateUser.setCheckEndTest(true);

            }
        }
        if (!isQuestionFound(user)/*Вопрос не найден , задаем вопрос пользовотилю*/) {
            updateUser.setQuestionNumber("1");
            updateUser.setLastMessageBot(getQuestionAndPreparationSendMessage());
        }


    }


    //----------Методы используються в этом класе-----------------------------------------------------------------------------------
    private void fieldInitializer(User user) {
        sendMessage = new SendMessage();
        tempReportJson = new ReportJson();
        updateUser = new User();
        updateUser.clone(user);
        //Для кнопок
        markupInline = new InlineKeyboardMarkup();
        rowsInline = new ArrayList<List<InlineKeyboardButton>>();
        rowInline = new ArrayList<InlineKeyboardButton>();
    }

    private boolean isQuestionFound(User user) {
        for (Questionnaire questionnaire : questionnaires) {
            if (questionnaire.getNameQuestionnaire().equals(user.getQuestionnaireName())) {
                for (String[] question : questionnaire.getQuestion()) {
                    if (question[0].equals(user.getQuestionNumber())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private String getQuestionAndPreparationSendMessage() {

        if (isText()) {
            sendMessage.setText(getQuestion()[2]);
        }

        if (isNumber()) {
            sendMessage.setText(getQuestion()[2]);
        }

        if (isButton()) {
            InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
            List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();

            for (int i = 3; i < getQuestion().length; i++) {
                rowInline.add(new InlineKeyboardButton().setText(getQuestion()[i]).setCallbackData(String.valueOf(i)));
            }
            rowsInline.add(rowInline);
            markupInline.setKeyboard(rowsInline);

            sendMessage.setReplyMarkup(markupInline);
            sendMessage.setText(getQuestion()[2]);
        }
        return getQuestion()[2];
    }

    private boolean isText() {
        if (getQuestion()[0].equals(updateUser.getQuestionNumber())) {//находим нужный вопрос
            if (getQuestion()[1].equals("text")) {
                return true;
            }
        }
        return false;
    }

    private boolean isButton() {
        if (getQuestion()[0].equals(updateUser.getQuestionNumber())) {//находим нужный вопрос
            if (getQuestion()[1].equals("button")) {
                return true;
            }
        }
        return false;
    }

    private boolean isNumber() {
        if (getQuestion()[0].equals(updateUser.getQuestionNumber())) {//находим нужный вопрос
            if (getQuestion()[1].equals("number")) {
                return true;
            }
        }
        return false;
    }

    private boolean isCheckConsent() {
        if (updateUser.isCheckConsent()) {
            return true;
        }
        return false;
    }

    private ReportJson getAnswer(AnswerToQuestion answerToQuestion) {
        tempReportJson.setToken("1");
        //tempReportJson.setAnswer();
        return tempReportJson;
    }

    private List<String[]> getQuestionsList() {
        for (Questionnaire questionnaire : questionnaires) {
            boolean temp = questionnaire.getNameQuestionnaire().equals(updateUser.getQuestionnaireName());
            if (temp) {//находим нужную анкету
                return questionnaire.getQuestion();
            }
        }
        return null;
    }

    private void setSendMessage(String message) {
        sendMessage.setText(message);//Задаем следующий вопрос

    }

    private void setAnswerToQuestion(String numberQuestion, String typeQuestion, String textQuestion, String answerQuestion) {
        answerToQuestion = new AnswerToQuestion();
        answerToQuestion.setToken(updateUser.getUserToken());
        answerToQuestion.setNumberQuestion(numberQuestion);
        answerToQuestion.setTypeQuestion(typeQuestion);
        answerToQuestion.setTextQuestion(textQuestion);
        answerToQuestion.setAnswerQuestion(answerQuestion);
    }

    private String[] getQuestion() {
        for (String[] question : getQuestionsList()) {
            if (question[0].equals(updateUser.getQuestionNumber())) {//находим нужный вопрос
                return question;
            }
        }
        return null;
    }

    private boolean checkQuestion(String lastQuestion) {
        Pattern p = Pattern.compile(checkAnswer);
        Matcher m = p.matcher(lastQuestion);
        return m.find();
    }

    //----------Методы возрата данных с данного класса----------------------------------------------------------------------------
    public SendMessage getSendMessage() {
        return sendMessage;
    }

    public User getUpdateUser() {
        return updateUser;
    }

    public List<Questionnaire> getQuestionnaires() {
        return questionnaires;
    }

}
