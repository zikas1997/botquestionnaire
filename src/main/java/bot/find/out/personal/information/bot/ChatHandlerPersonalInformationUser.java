package bot.find.out.personal.information.bot;

import bot.find.out.personal.information.entity.Questionnaire;
import bot.find.out.personal.information.entity.User;
import bot.find.out.personal.information.repo.UserPersonalInformationRepository;
import bot.find.out.personal.information.servise.PassingQuestionnaires;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

public class ChatHandlerPersonalInformationUser {

    //-----------------------------------------------------------

    private UserPersonalInformationRepository repository;
    private PassingQuestionnaires passingQuestionnaires;

    private SendMessage sendMessage;
    private String messageText;
    private long chatId;
    private String lastMessage;

    User user;

    //-----------------------------------------------------------

    public ChatHandlerPersonalInformationUser() {
        repository = new UserPersonalInformationRepository();
        passingQuestionnaires = new PassingQuestionnaires();

    }

    //-----------------------------------------------------------

    //Метод выбирает полходящий ответ, в рамках своей работы.
    public void choiceOfAnswerAndSaveInformation(User userDate){

        //-----------------------------------Test-----------------------------
            String[] mas = new String[]{"1","2","3"};
        //-----------------------------------------
        //Условия расположены в обратном порядке.
        fieldInitializer(userDate);

        if(isUserExit()){

            if(user.getUserToken()!=null){//Этот блок активен, если пользователь проше лрегистрацию

                chooseQuestionnaire();//Предлагаем выбрать анкету

                if(user.isCheckStartTest()){//Анкета выбрана, проходи тестирование!
                    passingQuestionnaires.checkWhyQuestion(user);
                    repository.updateUser(passingQuestionnaires.getUpdateUser());
                    sendMessage.setText(passingQuestionnaires.getSendMessage().getText());
                    sendMessage.setReplyMarkup(passingQuestionnaires.getSendMessage().getReplyMarkup());
                }

            }else {//Пользователь не прошел регистрацию.
                sendMessage.setText("Извините вы ввели не верный ключ доступа, попробйте ввести его заного или пройдите регистрацию на сайте ");
                for(String s: mas){
                    if(user.getLastMessageUser().equals(s)) {//Здесь сравниваем с нужным нам токеном
                        user.setUserToken(String.valueOf(user.getLastMessageUser()));
                        repository.updateUser(user);
                        sendMessage.setText("Вы успешно зарегистрированны, введите любое сообщение для продолжение.");
                    }
                }
            }

        }

        //Стартовое сообщение
        startMessage("Добрый день я бот. Чтобы пройти анектирование, зайдите в личный кабинет и узнайте ключ доступа.");


        if((user.getLastMessageUser().equals("ОтправитьQuestionnaires"))||(user.getLastMessageUser().equals("ПройтизановоQuestionnaires"))){
            repository.deleteUser(user.getChatId());
        }

    }

    public void addQuestionnaires(String jsonQuestionnaires){
        passingQuestionnaires.addQuestionnaires(jsonQuestionnaires);
    }

    private void fieldInitializer(User user){
        this.user  = new User();
        chatId = user.getChatId();
        lastMessage = user.getLastMessageBot();
        if(selectUser() != null){
            this.user.clone(repository.selectUser(user.getChatId()));
            this.user.setLastMessageUser(user.getLastMessageUser());
        }else{
            this.user.clone(user);
        }
        sendMessage = new SendMessage();
        sendMessage.setText("Упс!");
    }

    private boolean isUserExit(){
        if(selectUser() != null){
            return true;
        }
        else {
            return false;
        }
    }

    private User selectUser(){
        return repository.selectUser(chatId);
    }

    private void startMessage(String messageText){
        if(!isUserExit()){
            this.messageText = messageText;
            user.setLastMessageBot(messageText);
            repository.addUser(user);
            sendMessage.setText(getMessageReply());
        }
        if (selectUser() == null){
            this.messageText = messageText;
            user.setLastMessageBot(messageText);
            repository.addUser(user);
            sendMessage.setText(getMessageReply());
        }
    }

    private void chooseQuestionnaire(){

        if(selectUser().getQuestionnaireName().trim().length()==0 && selectUser().isCheckConsent()) {//Выбираем анкету и ставим пометку CheckStartTest = True
            user.setCheckConsent(false);
            user.setQuestionnaireName((getQuestionnaires().get(Integer.parseInt(user.getLastMessageUser())).getNameQuestionnaire()));
            user.setCheckStartTest(true);
            repository.updateUser(user);

            sendMessage.setText(user.getQuestionnaireName());

        }else {//Даем выбрать анкету
            if(selectUser().getQuestionnaireName().trim().length()==0 && !selectUser().isCheckConsent()) {
                //Делаем пометку(Что мы предлагаем выбрать одну из кнопок)
                user.setCheckConsent(true);

                InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> rowsInline = new ArrayList<List<InlineKeyboardButton>>();
                List<InlineKeyboardButton> rowInline = new ArrayList<InlineKeyboardButton>();

                for (int i = 0; i < passingQuestionnaires.getQuestionnaires().size(); i++) {
                    rowInline.add(new InlineKeyboardButton().setText(getQuestionnaires().get(i).getNameQuestionnaire()).setCallbackData(String.valueOf(i)));
                }
                rowsInline.add(rowInline);
                markupInline.setKeyboard(rowsInline);

                user.setLastMessageBot("Выбреите какую анкету вы хотите пройти?");
                sendMessage.setReplyMarkup(markupInline);
                sendMessage.setText(user.getLastMessageBot());
                repository.updateUser(user);
            }
        }
    }

    private List<Questionnaire> getQuestionnaires(){
         return passingQuestionnaires.getQuestionnaires();
    }
    //Возвращает подготовленый обйект SendMessage
    public SendMessage getSendMessage() {
        sendMessage.setChatId(chatId);
        return sendMessage;
    }

    public String getMessageReply() {
        return messageText;
    }



}
