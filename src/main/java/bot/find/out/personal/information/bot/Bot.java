package bot.find.out.personal.information.bot;

import bot.find.out.personal.information.entity.User;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

public class Bot  extends TelegramLongPollingBot{

    //-----------------------------------------------------------

    private ChatHandlerPersonalInformationUser chatHandlerPersonalInformationUser;
    private List<User> users = new ArrayList<User>();

    //-----------------------------------------------------------

    public Bot(){
        chatHandlerPersonalInformationUser = new ChatHandlerPersonalInformationUser();
    }

    //-----------------------------------------------------------

    public void onUpdateReceived(Update update) {

        if (update.hasMessage() && update.getMessage().hasText()) {

            User user = new User();
            user.setChatId(update.getMessage().getChatId());
            user.setLastMessageUser(update.getMessage().getText());

            chatHandlerPersonalInformationUser.choiceOfAnswerAndSaveInformation(user);

            try {
                execute(chatHandlerPersonalInformationUser.getSendMessage());
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
        else if (update.hasCallbackQuery()) {

            User user = new User();
            user.setChatId(update.getCallbackQuery().getMessage().getChatId());
            user.setLastMessageUser(update.getCallbackQuery().getData());
            chatHandlerPersonalInformationUser.choiceOfAnswerAndSaveInformation(user);

            try {
                execute(chatHandlerPersonalInformationUser.getSendMessage());
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    public String getBotUsername() {
        return "BotFindOutPersonalInformationbot";
    }

    public String getBotToken() {
        return "415952438:AAHC3neGUYT9qBpSDQH18Vcl0XiS47PYnbY";
    }

    public ChatHandlerPersonalInformationUser getChatHandlerPersonalInformationUser(){
        return chatHandlerPersonalInformationUser;
    }
}
