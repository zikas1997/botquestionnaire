package bot.find.out.personal.information;

import bot.find.out.personal.information.bot.Bot;
import bot.find.out.personal.information.entity.Questionnaire;
import com.google.gson.Gson;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.Arrays;

public class Start {

    public static void main(String[] args) {

        ApiContextInitializer.init();
        TelegramBotsApi botsApi = new TelegramBotsApi();

        //Чекаем запрос Gson
        Gson gson = new Gson();
        Bot bot = new Bot();

        //тестт1
        Questionnaire questionnaire = new Questionnaire("Анкета №1", Arrays.asList(new String[]{"1", "text", "Сам впорос: Кто проживает на дне океана?"},
                new String[]{"2", "button", "Сам впорос: Какую кнопку выберешь?","Кнопка 1" , "Кнопка 2" , "Кнопка3" , "Копка4"},
                new String[]{"3", "number", "Сам впорос: Сколько тебе лет?"},
                new String[]{"4", "button", "Сам впорос: Сколько тебе лет?","Да"},
                new String[]{"5", "number", "Сам впорос: Сколько стоит батон?"},
                new String[]{"6", "text", "Сам впорос: Как дела?"}));
        String json = gson.toJson(questionnaire);
        bot.getChatHandlerPersonalInformationUser().addQuestionnaires(json);

        Questionnaire questionnaire3 = new Questionnaire("Анкета №3", Arrays.asList(new String[]{"1", "text", "Сам впорос: Кто я?"},
                new String[]{"2", "number", "Сам впорос: Сколько тебе лет?"},
                new String[]{"3", "button", "Сам впорос: Какую кнопку выберешь?","Кнопка 1" , "Кнопка2" , "Кнопка3" , "Копка4"}));
        String json3 = gson.toJson(questionnaire3);
        bot.getChatHandlerPersonalInformationUser().addQuestionnaires(json3);


        Questionnaire questionnaire2 = new Questionnaire("Анкета №2", Arrays.asList(new String[]{"1", "button", "Сам впорос: Какую кнопку выберешь?","Кнопка 1" , "Кнопка2" , "Кнопка3" , "Копка4"},
                new String[]{"2", "text", "Сам впорос: Кто я?"},
                new String[]{"3", "number", "Сам впорос: Сколько тебе лет?"}));
        String json2 = gson.toJson(questionnaire2);
        bot.getChatHandlerPersonalInformationUser().addQuestionnaires(json2);

        //
        try {
            botsApi.registerBot(bot);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
